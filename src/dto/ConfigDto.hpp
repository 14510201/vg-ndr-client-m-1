#ifndef ConfigDto_hpp
#define ConfigDto_hpp

#include "oatpp/core/macro/codegen.hpp"
#include "oatpp/core/Types.hpp"

#include OATPP_CODEGEN_BEGIN(DTO)

/**
 * Config DTO.
 * {
        "redis_ip": <redis ip>,
        "redis_port": <redis port>,
        "interface" : <interface>,
        "gw_eui" : <GW EUI>,
        "mode": <fsk | scte-25-1>
    }
 */
class ConfigDto : public oatpp::DTO {
  
  DTO_INIT(ConfigDto, DTO)
  DTO_FIELD(String, interface, ConfigDto::INTERFACE_NAME.c_str());
  DTO_FIELD(String, gatewayId, ConfigDto::GATEWAY_ID.c_str());
  DTO_FIELD(String, redisIp, ConfigDto::REDIS_IP.c_str());
  DTO_FIELD(UInt32, redisPort, ConfigDto::REDIS_PORT.c_str());
  DTO_FIELD(String, mode, ConfigDto::MODE.c_str());

  static std::string REDIS_IP;
  static std::string REDIS_PORT;
  static std::string GATEWAY_ID;
  static std::string INTERFACE_NAME;
  static std::string MODE;
  static std::string API_PORT;

};

#include OATPP_CODEGEN_END(DTO)

#endif /* ConfigDto_hpp */