#include "ConfigDto.hpp"

std::string ConfigDto::REDIS_IP = "redis-ip";
std::string ConfigDto::REDIS_PORT = "redis-port";
std::string ConfigDto::GATEWAY_ID = "gateway-id";
std::string ConfigDto::INTERFACE_NAME = "interface";
std::string ConfigDto::MODE = "mode";
std::string ConfigDto::API_PORT = "api-port";

