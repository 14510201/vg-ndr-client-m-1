#ifndef NDR_CLIENT_H
#define NDR_CLIENT_H

#include "services/INdrDataSrc.hpp"
#include "utils/ConfigService.hpp"
#include "services/NdrRedisClient.hpp"
#include "services/NdrFskDemodulator.hpp"

class NDRClient {

private:
    INDRDataSrc *ndrDataSrc;
    ConfigService *config;
    NdrRedisClient *redisClient;
    NdrFskDemodulator *fskDemodulator;
    bool stop = false;
    bool running = false;
    user_api::SvcFskModemConnectionUserApi userConnect;
    void run();

public:
    NDRClient(INDRDataSrc * ndrDataSrc);
    ~NDRClient();
    void Initialize();
    void Start();
    void Shutdown();
};

#endif