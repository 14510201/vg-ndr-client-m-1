#include "Logging.hpp"

Logging::Logging() {
    log4cpp::PropertyConfigurator::configure("log4cpp.properties");
    log4cpp::Category::getRoot().setPriority(log4cpp::Priority::INFO);
}

log4cpp::Category& Logging::getLogger(std::string category) {
    return log4cpp::Category::getInstance(category);
}

Logging::~Logging() {
    log4cpp::Category::shutdown();
}