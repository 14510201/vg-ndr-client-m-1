#ifndef NDR_REDIS_CLIENT_H
#define NDR_REDIS_CLIENT_H

#include <iostream>
#include <boost/program_options.hpp>
#include <sw/redis++/redis++.h>
#include <sw/redis++/redis_cluster.h>
#include "../utils/Logging.hpp"

class NdrRedisClient
{
    sw::redis::Redis redisClient;
    std::string queueName;
    bool stop = false;
    
public:
    NdrRedisClient(const std::string ip, uint32_t port, const std::string queueName);
    ~NdrRedisClient();
    inline log4cpp::Category& getLogger(){
        return Logging::getInstance().getLogger("NdrRedisClient");
    }

    std::string getTimeUTCCompactFormat();

    void SendMessage(const std::string& value, int chanNum = 0);
};

#endif
