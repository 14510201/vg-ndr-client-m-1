#include "NdrDataFactory.hpp"
#include "FileDataSrc.hpp"
#include "L2TPv3SocketDataSrc.hpp"

INDRDataSrc* NdrDataFactory::CreateFromFile(std::string& filename){
    return new FileDataSrc(filename);
}

INDRDataSrc* NdrDataFactory::CreateFromInterface(std::string& interface){
    return new L2TPv3SocketDataSrc(interface);
}