


#include "NdrRedisClient.hpp"
#include <sw/redis++/redis.h>
#include <iomanip>
#include <nlohmann/json.hpp>


NdrRedisClient::NdrRedisClient(const std::string ip, uint32_t port, const std::string queueName)
    : redisClient("tcp://" + ip + ":" + std::to_string(port))
{
    this->queueName = queueName;
}

NdrRedisClient::~NdrRedisClient(){

}

std::string NdrRedisClient::getTimeUTCCompactFormat(){
    auto now = std::chrono::system_clock::now().time_since_epoch();
    auto now_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(now).count();

    // Convert the time to ISO 8601 compact format
    std::time_t now_time_t = now_microseconds / 1000000;
    std::tm* now_tm = std::gmtime(&now_time_t);
    char iso_format[20]; // This is the size for the YYYYMMDDTHHMMSS format
    std::strftime(iso_format, sizeof(iso_format), "%Y%m%dT%H%M%S", now_tm);

    // Format the microseconds
    int microseconds = now_microseconds % 1000000;
    std::cout << std::setw(6) << std::setfill('0') << microseconds;

    std::ostringstream oss;
    oss << iso_format << 'Z';
    std::string iso_format_str = oss.str();
    return iso_format_str;
}

/** 
 * Generate a json with 
 * time: utc compact format, 
 * tmst: microseconds since epoch, 
 * payload: hex string of the value, 
 * ch: channel 
**/


void NdrRedisClient::SendMessage(const std::string& value, int chanNum){
    std::string iso_format_str = this->getTimeUTCCompactFormat();
    auto now_ms = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    uint32_t now_ms_uint32 = static_cast<uint32_t>(now_ms);
    getLogger().info("Sending message to queue: {} with value: {}", this->queueName, value);
    nlohmann::json j = {
        {"data", value},
        {"time", iso_format_str},
        {"tmst", now_ms_uint32},
        {"channel", chanNum}
    };
    std::string json_str = j.dump();
    getLogger().info("Sending message with payload: {}", json_str);

    this->redisClient.lpush(this->queueName, json_str);
}



