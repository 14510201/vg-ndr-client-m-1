#include <iostream>
#include <thread>
#include <iomanip>
#include <nlohmann/json.hpp>
#include <boost/chrono/chrono.hpp>
#include <boost/thread/thread.hpp>
#include "NdrFskDemodulator.hpp"
#include <openssl/bio.h>
#include <openssl/types.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <cstring>

using namespace std;

std::string NdrFskDemodulator::CFG_GFSK_50KBPS_ALT = "CFG_GFSK_50KBPS_ALT";
std::string NdrFskDemodulator::CFG_GFSK_50KBPS = "CFG_GFSK_50KBPS";

NdrFskDemodulator::NdrFskDemodulator(svc_fsk_modem::user_api::SvcFskModemConnectionUserApi *pUserConnect, 
    NdrRedisClient *ndrRedisClient){
    this->ndrRedisClient = ndrRedisClient;
    this->pUserConnect = pUserConnect;
    this->logger = &log4cpp::Category::getInstance("NdrFskDemodulator");
}

NdrFskDemodulator::~NdrFskDemodulator(){
    log4cpp::Category::shutdown();
}

void NdrFskDemodulator::_SetConfiguration(std::string cfgName)
{
	// send cmd, await completion response
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdSetConfiguration_t()) };
	user_api::CmdSetConfiguration_t& cmdConfig = cmd.setConfiguration;
	cmdConfig.config.Name(cfgName);
	cmdConfig.config.mod.pulseShapingBt = 1.0;
	this->pUserConnect->SendCommand(cmd);
	user_api::SvcFskModemResponse_t resp;
	while (this->pUserConnect->ReceiveResponse(resp) != 0);

    // assuming you have a logger instance named `logger`
    logger->info("Configuration Set: {}", cfgName);
}

void NdrFskDemodulator::_ServiceAttach()
{
	char versionStr[32];
	char connectionVersionStr[32];
	{
		std::lock_guard<std::mutex> guard(mutCout);
        this->logger->info("FskDemodulator Initializing...");
	}

	while (this->pUserConnect->Attach() < 0)
	{
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}

	// continually ping the service, wait for reply.
	bool ready = false;
	user_api::SvcFskModemCommand_t cmd{user_api::SvcFskModemCommand_t(user_api::CmdPing_t())};
	user_api::SvcFskModemResponse_t resp;
	do
	{
		this->pUserConnect->SendCommand(cmd);
		if (this->pUserConnect->ReceiveResponse(resp) == 0)
		{
			ready = (resp.Type() == user_api::ResponseType_t::Ready);
		}
	} while (!ready);
    this->logger->info("Attached to SvcFskModem...");
	// Show versions
	{
		std::lock_guard<std::mutex> guard(mutCout);
        this->logger->info("Service Vers: %s", this->pUserConnect->GetVersion().c_str());
        this->logger->info("Connection Vers: %s", this->pUserConnect->GetConnectionVersion().c_str());
	}
};

void NdrFskDemodulator::_GetActiveChannels()
{
	// send cmd, await response(s).
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdListChannels_t()) };
	user_api::SvcFskModemResponse_t resp;
	pUserConnect->SendCommand(cmd);

	std::lock_guard<std::mutex> guard(mutCout);
    this->logger->info("Active Channels List:");
	bool done;
	do {
		while (pUserConnect->ReceiveResponse(resp) != 0);
		user_api::RespActiveChannelsListEntry_t activeChan = resp.activeChannel;

		// display
        this->logger->info("\tChan Number: %d   Center Frequency (kHz): %-7.0f", activeChan.number, activeChan.centerFrequency / 1000.00);
		done = activeChan.last;
	} while (!done);
}

void NdrFskDemodulator::_ShowActiveConfiguration()
{
	// send cmd, await response
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdGetConfiguration_t()) };
	user_api::SvcFskModemResponse_t resp;
	this->pUserConnect->SendCommand(cmd);
	while (this->pUserConnect->ReceiveResponse(resp) != 0);
	user_api::RespConfigurationReport_t configReport = resp.configurationReport;
	// display
	std::lock_guard<std::mutex> guard(mutCout);
    std::ostringstream oss;
    oss << "Active Config: \"" << configReport.config.Name() << "\"  Mod BT = " << configReport.config.mod.pulseShapingBt;
    this->logger->info(oss.str());
}

void NdrFskDemodulator::_DiagnosticsEnable(bool enable)
{
	// send cmd, await completion response
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdSetDiagnostics_t()) };
	user_api::CmdSetDiagnostics_t& cmdDiags = cmd.setDiagnostics;
	cmdDiags.demod.enable = enable;
	cmdDiags.mod.enable = enable;
	this->pUserConnect->SendCommand(cmd);
	user_api::SvcFskModemResponse_t resp;
	while (this->pUserConnect->ReceiveResponse(resp) != 0);

	if (enable)
	{
		std::cout << "Diagnostics Enabled" << std::endl;
	}
	else
	{
		std::cout << "Diagnostics Disabled" << std::endl;
	}
}

void NdrFskDemodulator::Initialize(){
    this->_ServiceAttach();
    //this->_SetConfiguration(NdrFskDemodulator::CFG_GFSK_50KBPS);
    this->_GetActiveChannels();
    this->_ShowActiveConfiguration();
}

std::string NdrFskDemodulator::base64_encode(const unsigned char* input, size_t length) {
    BIO* bmem = BIO_new(BIO_s_mem());
    BIO* b64 = BIO_new(BIO_f_base64());
    BIO_push(b64, bmem);
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

    // Write the bytes to the BIO
    BIO_write(b64, input, length);
    BIO_flush(b64);

    // Get the output buffer
    BUF_MEM* bptr;
    BIO_get_mem_ptr(b64, &bptr);

    // Create a string from the buffer
    std::string result(bptr->data, bptr->length);

    // Cleanup
    BIO_free_all(b64);

    return result;
}

void NdrFskDemodulator::StartDemodPktListenerThread(void)
{
    this->running = true;
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    uint8_t packet[256];
    int pktLength;
    int pktCount = 0;
    uint8_t chanNum;
    
    while (!this->stop)
    {
        try
        {
            if ((pktLength = this->pUserConnect->ReceiveDemodPacket((void *)packet, 256, &chanNum)) > 0)
            {	
				std::string base64str = base64_encode(packet, pktLength);
				this->logger->info("Packet received (Len " + 
					std::to_string(pktLength)+ 
					", Channel " + std::to_string(chanNum) + "): "+base64str);

                std::stringstream ss;

                for (int i = 0; i < pktLength; i++)
                {
                    ss << std::setfill('0') << std::hex << std::setw(2) << (unsigned int)std::uint8_t(packet[i]);
                    if ((i % 8) == 0)
                    {
                        std::cout << std::endl
                                  << "\t\t";
                    }
                    std::cout << std::hex << "0x" << std::setw(2) << std::setfill('0') << (int)packet[i];
                    if (i != pktLength - 1)
                    {
                        std:cout << ", ";
                    }
                }
				std::cout << std::endl << std::endl;
                this->logger->debug("Packet sent to Redis (hex) : "+ ss.str());
                ndrRedisClient->SendMessage(base64str, chanNum);
                pktCount++;
            }
        }
        catch (...)
        {
            std::exception_ptr exceptPtr;
            this->logger->error("NdrDataReceiver Run Exception:");
            running = false;
            exceptPtr = std::current_exception();
        }
    }
    this->running = false;
    std::cout << "Packet count: " << std::dec << pktCount << std::endl
              << std::endl;
}

void NdrFskDemodulator::Stop(){
    this->_DiagnosticsEnable(false);
    this->stop = true;
}
