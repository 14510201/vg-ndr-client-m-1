# VLGW NDR Client

    NDRClient -f /tmp/ndr_join_only_3_pkts.pcapng -S 0x80001ff1 -s 112.97.0.38 -d 112.97.0.6 -r 192.168.44.81 -p 6379 -g eb9a02ab45feb421

    sudo /home/atl_lab/bin/NDRClient -i enp2s0.1 -S 0x80001ff1 -s 112.97.0.38 -d 112.97.0.6 -r 192.168.44.81 -p 6379 -g eb9a02ab45feb421


# Prerequisite
    PcapPlusPlus
    Redis
    oatpp/oatpp-swagger

## Install oatpp for Swagger API support in c++

    # https://github.com/oatpp/oatpp

    git clone https://github.com/oatpp/oatpp
    cd oatpp/
    mkdir build && cd build/
    cmake ..
    make install

    git clone https://github.com/oatpp/oatpp-swagger
    cd oatpp-swagger/
    mkdir build && cd build/
    cmake ..
    make install
    
# how to check logs

    journalctl -u vg-ndr-client.service -f

# NDR Client Roadmap

## R1. The NDR client currently supports single Session ID, it should support configuration of these parameters using the API

    GET /config
    {
        "redis_ip": <redis ip>,
        "redis_port": <redis port>,
        "interface" : <interface>,
        "gw_eui" : <GW EUI>,
        "mode": <fsk | scte-25-1>
    }

    PUT /config
    {
        "redis_ip": <redis ip>,
        "redis_port": <redis port>,
        "interface" : <interface>,
        "gw_eui" : <GW EUI>,
        "mode": <fsk | scte-25-1>
    }
    
    GET /sessions - get all sessions
    [
        {
            "src": <src ip>,
            "dest": <dest ip>,
            "session_id" : <session id>
        }
    ]

    POST /sessions - create new session
    {
        "src": <src ip>,
        "dest": <dest ip>,
        "session_id" : <session id>
    }

    DELETE /sessions/{session_id} - delete a session

    GET /status
    {
        "status": "running"
    }

    PUT /status
    {
        "status" : "<start> | <stop>"
    }

    GET /logs - get the log file


## R2 Improve the handling the message with tmst 