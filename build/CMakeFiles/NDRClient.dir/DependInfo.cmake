
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/linuxuser/aoi/vlgw/2024-04-10/vg-ndr-client/src/NdrClient.cpp" "CMakeFiles/NDRClient.dir/src/NdrClient.cpp.o" "gcc" "CMakeFiles/NDRClient.dir/src/NdrClient.cpp.o.d"
  "/home/linuxuser/aoi/vlgw/2024-04-10/vg-ndr-client/src/controller/ErrorHandler.cpp" "CMakeFiles/NDRClient.dir/src/controller/ErrorHandler.cpp.o" "gcc" "CMakeFiles/NDRClient.dir/src/controller/ErrorHandler.cpp.o.d"
  "/home/linuxuser/aoi/vlgw/2024-04-10/vg-ndr-client/src/dto/ConfigDto.cpp" "CMakeFiles/NDRClient.dir/src/dto/ConfigDto.cpp.o" "gcc" "CMakeFiles/NDRClient.dir/src/dto/ConfigDto.cpp.o.d"
  "/home/linuxuser/aoi/vlgw/2024-04-10/vg-ndr-client/src/services/FileDataSrc.cpp" "CMakeFiles/NDRClient.dir/src/services/FileDataSrc.cpp.o" "gcc" "CMakeFiles/NDRClient.dir/src/services/FileDataSrc.cpp.o.d"
  "/home/linuxuser/aoi/vlgw/2024-04-10/vg-ndr-client/src/services/L2TPv3SocketDataSrc.cpp" "CMakeFiles/NDRClient.dir/src/services/L2TPv3SocketDataSrc.cpp.o" "gcc" "CMakeFiles/NDRClient.dir/src/services/L2TPv3SocketDataSrc.cpp.o.d"
  "/home/linuxuser/aoi/vlgw/2024-04-10/vg-ndr-client/src/services/NdrDataFactory.cpp" "CMakeFiles/NDRClient.dir/src/services/NdrDataFactory.cpp.o" "gcc" "CMakeFiles/NDRClient.dir/src/services/NdrDataFactory.cpp.o.d"
  "/home/linuxuser/aoi/vlgw/2024-04-10/vg-ndr-client/src/services/NdrFskDemodulator.cpp" "CMakeFiles/NDRClient.dir/src/services/NdrFskDemodulator.cpp.o" "gcc" "CMakeFiles/NDRClient.dir/src/services/NdrFskDemodulator.cpp.o.d"
  "/home/linuxuser/aoi/vlgw/2024-04-10/vg-ndr-client/src/services/NdrRedisClient.cpp" "CMakeFiles/NDRClient.dir/src/services/NdrRedisClient.cpp.o" "gcc" "CMakeFiles/NDRClient.dir/src/services/NdrRedisClient.cpp.o.d"
  "/home/linuxuser/aoi/vlgw/2024-04-10/vg-ndr-client/src/services/PspOobPacket.cpp" "CMakeFiles/NDRClient.dir/src/services/PspOobPacket.cpp.o" "gcc" "CMakeFiles/NDRClient.dir/src/services/PspOobPacket.cpp.o.d"
  "/home/linuxuser/aoi/vlgw/2024-04-10/vg-ndr-client/src/services/PspOobPcapng.cpp" "CMakeFiles/NDRClient.dir/src/services/PspOobPcapng.cpp.o" "gcc" "CMakeFiles/NDRClient.dir/src/services/PspOobPcapng.cpp.o.d"
  "/home/linuxuser/aoi/vlgw/2024-04-10/vg-ndr-client/src/services/UserService.cpp" "CMakeFiles/NDRClient.dir/src/services/UserService.cpp.o" "gcc" "CMakeFiles/NDRClient.dir/src/services/UserService.cpp.o.d"
  "/home/linuxuser/aoi/vlgw/2024-04-10/vg-ndr-client/src/utils/AppException.cpp" "CMakeFiles/NDRClient.dir/src/utils/AppException.cpp.o" "gcc" "CMakeFiles/NDRClient.dir/src/utils/AppException.cpp.o.d"
  "/home/linuxuser/aoi/vlgw/2024-04-10/vg-ndr-client/src/utils/ConfigService.cpp" "CMakeFiles/NDRClient.dir/src/utils/ConfigService.cpp.o" "gcc" "CMakeFiles/NDRClient.dir/src/utils/ConfigService.cpp.o.d"
  "/home/linuxuser/aoi/vlgw/2024-04-10/vg-ndr-client/src/utils/Logging.cpp" "CMakeFiles/NDRClient.dir/src/utils/Logging.cpp.o" "gcc" "CMakeFiles/NDRClient.dir/src/utils/Logging.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
